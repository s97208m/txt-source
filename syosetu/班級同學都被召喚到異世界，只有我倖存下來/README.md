# novel

- title: クラスが異世界召喚されたなか俺だけ残ったんですが
- title_zh1: 班級同學都被召喚到異世界，只有我幸存下來
- author: サザンテラス
- illust:
- source: http://ncode.syosetu.com/n1619dr/
- cover:
- publisher: syosetu
- date: 2019-04-01T22:52:00+08:00
- status: 連載
- novel_status: 0x0000

## illusts


## publishers

- syosetu

## series

- name: クラスが異世界召喚されたなか俺だけ残ったんですが

## preface


```
在教室裡突然出現的神的委託，全班全部被強制召喚到了異世界！
但是，在角落裡睡覺的神谷夜兔沒有進入魔法陣，只被授予了技能，就留在了現實世界。
成為高中生，平穩的度過每天的決心，學校恐怖分子被佔領，
或是從異世界怪獸出現的話夜兔的居住的街道紛紛騷動了……。
無精打采的少年拯救世界嗎？

【2017/9/30 モンスター文庫様より書籍化しました。また、デンシバーズ様にてコミカライズ連載が決定しました！】

突然異世界の神からの半ば強制的な依頼で俺、神谷夜兎(かみややと)のいるクラスは神によって異世界召喚された。ーーーーーーーーーーー俺以外は。
教室の端っこの席で寝ていた俺は円い魔法陣の中に運よく入ることなく地球に留まりスキルだけ授かる。それから警察の事情聴取やらテレビの報道などなんやかんやあって一年経ちーーーーー俺は高校生になった


ーー感想は読むだけで基本返信致しませんーー
```

## tags

- node-novel
- R15
- syosetu
- ご都合主義
- ほのぼの
- ギャグ
- チート
- ネット小説大賞五
- ファンタジー
- ラブコメ
- ローファンタジー
- ローファンタジー〔ファンタジー〕
- 主人公最強
- 多分ハーレム
- 学園
- 日常
- 残酷な描写あり
- 現代
- 男主人公
- 青春
- 魔法

# contribute

- N紫Fu-ji
- 暮宥壬
- E世界大家都睡
- 中年油腻肥宅
- 阳都key
- HSbF6-
- 黯伤修罗殿
- 休闲飛
- s102337938
- ReeVince
- 章魚
- I法克灬油
- 嘎嘎的丫丫2b
- 肥宅戰神
- 蟻
- 梦蚀
- a221550903
- 

# options

## downloadOptions

- noFilePadend: true
- filePrefixMode: 1
- startIndex: 1

## syosetu

- txtdownload_id:
- series_id:
- novel_id: n1619dr

## textlayout

- allow_lf2: true

# link

- [narou.nar.jp](https://narou.nar.jp/search.php?text=n1619dr&novel=all&genre=all&new_genre=all&length=0&down=0&up=100) - 小説家になろう　更新情報検索
- [小説情報](https://ncode.syosetu.com/novelview/infotop/ncode/n1619dr/)
- http://www.dm5.com/manhua-banjitongxuedubeizhaohuandaoyishijie-zhiyouwoxingcunxialai/
- [班级同学都被召唤到异世界吧](https://tieba.baidu.com/f?kw=%E7%8F%AD%E7%BA%A7%E5%90%8C%E5%AD%A6%E9%83%BD%E8%A2%AB%E5%8F%AC%E5%94%A4%E5%88%B0%E5%BC%82%E4%B8%96%E7%95%8C&ie=utf-8&tp=0 "班级同学都被召唤到异世界")
- 

